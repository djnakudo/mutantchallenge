
const express = require('express')
const app = express()
const router = require('./server/routes')
const port = process.env.PORT || 8080
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', router);

app.listen(port,()=>{
    console.log(`Server started on port : ${port}`);
})

module.exports=app;





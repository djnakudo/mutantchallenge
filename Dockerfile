FROM node:lts-alpine as prod

EXPOSE 8080

RUN apk add --no-cache tini 

WORKDIR /usr/src/app

COPY package.json package-lock*.json ./

RUN npm install --only=prod && npm cache clean --force

COPY . .

ENTRYPOINT ["/sbin/tini", "--"]

CMD [ "node", "index.js" ]

FROM prod as dev
ENV NODE_ENV=development
RUN npm i --only=development
CMD ["./node_modules/nodemon/bin/nodemon.js","index","--inspect=0.0.0.0:9229"]

FROM dev as test
ENV NODE_ENV=test
CMD ["npm","run","test"]
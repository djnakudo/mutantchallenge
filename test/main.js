let chai = require("chai");
let chaiHttp = require("chai-http");
let should = chai.should();
let server = require("../index");

chai.use(chaiHttp);

describe("Users", () => {
  describe("/GET users analysis", () => {
    it("it should GET all the users websites", done => {
      chai
        .request(server)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.to.have.all.keys(
            "websites",
            "name_email_company",
            "with_suite"
          );
          res.body.websites.should.be.a("array");
          //checks if all websites are strings
          expect(res.body.websites).to.satisfy(function(websites) {
            return websites.every(function(website) {
              return website instanceof String;
            });
          });

          res.body.name_email_company.should.be.a("array");
          //checks if all name_Email_company  includes name,company and email as props
          expect(res.body.name_email_company).to.satisfy(function(
            name_email_company
          ) {
            return name_email_company.every(function(user) {
              return (
                user.name !== undefined &&
                user.company !== undefined &&
                user.email !== undefined
              );
            });
          });

          //check if it is ordered by name alphabetically
          expect(res.body.name_email_company).to.satisfy(function(
            name_email_company
          ) {
            return name_email_company
              .map(el => el.name)
              .slice(1)
              .every((item, i) => arr[i] <= item);
          });

          res.body.with_suite.should.be.a("array");
          //check if it all users includes suite in the address
          expect(res.body.with_suite).to.satisfy(function(with_suite) {
            return with_suite.every(function(user) {
              return (
                user.address.street.toLowerCase().includes("suite") ||
                user.address.suite.toLowerCase().includes("suite") ||
                user.address.city.toLowerCase().includes("suite")
              );
            });
          });

          done();
        });
    });
  });
});

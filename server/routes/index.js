const express = require("express");
const router = express.Router();
const API_ROOT = " https://jsonplaceholder.typicode.com/users";
const https = require("https");
const { elasticLogging } = require("../middlewares/elasticsearch");

// returns analysis from users
router.get("/", function(req, res,next) {
  //making http request for the given api
  https
    .get(API_ROOT, resp => {
      let data = "";

      // A chunk of data has been recieved.

      const { statusCode } = resp;
      if (statusCode === 200) {
        resp.on("data", chunk => {
          data += chunk;
        });

        resp.on("end", () => {
          const users = JSON.parse(data);
          //map only websites from each users
          const websites = users.map(el=>el.website)
          //sort users by name,email and company name
          const sortedUsersByName = users.map(el=>({name:el.name,email:el.email,company:el.company})).sort(function(a, b){ if(a.name.split(" ")[0] < b.name.split(" ")[0] ){
            return -1;
        }
       if (a.name.split(" ")[0] > b.name.split(" ")[0] ){
            return 1;
        }
       if (a.name.split(" ")[1] < b.name.split(" ")[1] ){
            return -1;
        }
       if (a.name.split(" ")[1] > b.name.split(" ")[1] ){
            return 1;
        }
        if(a.email < b.email ){
            return -1;
        }
       if (a.email > b.email ){
            return 1;
        }
        if(a.company.name < b.company.name ){
            return -1;
        }
       if (a.company.name > b.company.name ){
            return 1;
        }
        
        });
          //only users with suite in address
          const withSuite = users.filter(el=>{
              const {address} = el;
              if(address.street.toLowerCase().includes('suite')||
              address.suite.toLowerCase().includes('suite')||
              address.city.toLowerCase().includes('suite')) return true;
              else return false;
          });
          let dataSend= {
            websites,
            name_email_company:sortedUsersByName,
            with_suite:withSuite
          }
          res.status(200).json(dataSend)
          res.locals.reqData = {
              status:200,
              data:JSON.stringify(dataSend)
          }
          next();

        });
      } else {
          let dataSend={
            error: resp.statusMessage
          }
        res.status(500).json(dataSend);
        res.locals.reqData = {
              status:500,
              data:JSON.stringify(dataSend)
          }
        next();
      }
    })
    .on("error", err => {
        let dataSend={
            error: err
          }
      res.status(500).json(dataSend);
      res.locals.reqData = {
              status:500,
              data:JSON.stringify(dataSend)
          }
      next();
    });
});
//enables logging for the routes only in production or development
if(process.env.NODE_ENV!=='test')
router.use(elasticLogging);

module.exports = router;

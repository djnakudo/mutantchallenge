const client = require("../elasticsearch/index");

// middleware for elasticsearch logging
async function elasticLogging(req,res,next) {
  const {reqData} = res.locals;

    // arguments[0] (or `data`) contains the response body
    console.log(reqData)
    let currentDate = new Date()
    if(!reqData) return next();
        console.log(reqData);
        //creates,if not already created, elastic loging main index
        try {
         if(!await client.indices.exists({index:'requests'}))
          await client.indices.create({
            index: "requests"
          });
        } catch (error) {}
    
        await client.index({
          index: "requests",
         type:'mainRoute',
         id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
          body: {
            status:reqData.status,
            data: reqData.data,
            date:currentDate
          }
        });
   
   

 
 
  next();
}

module.exports = {
  elasticLogging
};

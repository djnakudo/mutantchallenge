#Requirements
    - [Vagrant](https://www.vagrantup.com/) 
    - [vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
    - might need to disable firewall for testing purposes

#Requirements(Alternative)
     - [docker](https://www.docker.com/) 
    - [docker-compose](https://docs.docker.com/compose/)

# Installation
    #Using Vagrant:
  - To start the app run on cmd line: vagrant up
  - To stop the services : vagrant destroy
  - to access the api go to : http://192.168.192.168
   #Using Docker-compose:
  - To start the app run on cmd line: docker-compose up
  - To stop the services : docker-compose down
  - to access the api go to : http://localhost